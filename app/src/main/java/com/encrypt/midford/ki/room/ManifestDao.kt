package com.encrypt.midford.ki.room

import androidx.room.*

@Dao
interface ManifestDao {

    @Query("SELECT * FROM manifest LIMIT 1")
    fun getManifest(): Manifest?

    @Insert
    fun insertManifest(manifest: Manifest)

    @Update
    fun updateManifest(manifest: Manifest)

    @Delete
    fun deleteManifest(manifest: Manifest)
}
