package com.encrypt.midford.ki.utils;

public enum FileType {
    DIRECTORY(1), AUDIO(2), DOC(3), GENERIC(4), IMAGE(5)
        , VIDEO(6), ARCHIVE(7), PDF(8);

    public static final String TAG = "Type";

    public static final String[] IMAGE_FORMATS = {"ani", "anim", "apng", "art", "bmp", "bpg", "gif"
            , "ico", "ics", "jpg", "jpeg", "miff", "png", "rgbe", "tiff", "sgi", "tga", "ufo", "ufp"
            , "wbmp", "webp", "svg"};
    public static final String[] AUDIO_FORMATS = {"3gp", "aa", "aac", "flac", "m4a", "m4b", "mp3"
            , "ogg", "oga", "mogg", "ra", "rm", "raw", "wav", "wma", "webm"};
    public static final String[] VIDEO_FORMATS = {"wmv", "webm", "vob", "rm", "ogv", "ogg", "nsv"
            , "mpg", "mpeg", "m2v", "mp2", "mpe", "mpv", "mp4", "m4p", "m4v", "mov", "qt", "mkv"
            , "gifv", "flv", "f4v", "f4p", "f4a", "f4b", "avi", "3gp"};
    public static final String[] DOC_FORMATS = {"doc", "docx", "odt", "pages", "rtf", "wpd"
            , "wp", "wp7", "txt"};
    public static final String[] ARCHIVE_FORMATS = {"vip", "comppkg", "b6z", "s00", "dar", "hki"
            , "rte", "mpkg", "pup", "rar", "pwa", "cdz", "deb", "gzip", "xapk", "zpi", "lemon", "sit"
            , "sy_", "zip", "pkg", "piz", "7z", "uha", "cbr", "arduboy", "bh", "dl_", "ecs", "fzpz"
            , "ice", "lzm", "mint", "pbi", "pit", "qda", "r00", "r2", "rp9", "s02", "sifz", "smpf"
            , "sqx", "tar", "tcx", "dz", "nex", "b1", "bndl", "bz2", "par", "pf", "gz", "f", "kgb"
            , "pak", "fdp", "rev", "xip", "lzma", "zix", "rpm", "jsonlz4", "a02", "c00", "ita", "jar"
            , "cbz", "tx_", "pea"};
    public static final String[] PDF_FORMATS = {"pdf"};

    private final int value;

    FileType(int value) {
        this.value = value;
    }

    public int toInt() {
        return value;
    }

    public static FileType fromInt(int i) {
        for (FileType t : FileType.values())
            if (t.toInt() == i)
                return t;
        return null;
    }
}
