package com.encrypt.midford.ki.encrypt;

import android.os.Parcel;
import android.os.Parcelable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Key implements Parcelable {

    private static final int KEY_LENGTH = 256;
    private static final int ITERATIONS = 65536;

    private SecretKey key;
    private String algorithm;
    private String digest;
    private byte[] salt;

    public Key(byte[] salt, char[] phrase, String algorithm, String digest) throws EncryptException {
        this.salt = salt;
        this.algorithm = algorithm;
        this.digest = digest;
        this.key = generateSecretKey(salt, phrase, algorithm);
    }

    private Key(Parcel parcel) {
        salt = parcel.createByteArray();
        byte[] encoded = parcel.createByteArray();
        algorithm = parcel.readString();
        digest = parcel.readString();

        key = new SecretKeySpec(encoded, algorithm);
    }

    public static final Creator<Key> CREATOR = new Creator<Key>() {
        @Override
        public Key createFromParcel(Parcel in) {
            return new Key(in);
        }

        @Override
        public Key[] newArray(int size) {
            return new Key[size];
        }
    };

    // Methods

    private SecretKey generateSecretKey(byte[] salt, char[] phrase, String algorithm) throws EncryptException {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(algorithm);
            KeySpec spec = new PBEKeySpec(phrase, salt, ITERATIONS, KEY_LENGTH);
            return factory.generateSecret(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new EncryptException(e.getMessage());
    } }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByteArray(salt);
        dest.writeByteArray(key.getEncoded());
        dest.writeString(algorithm);
        dest.writeString(digest);
    }

    // Getters & setters

    public String getAlgorithm() {
        return algorithm;
    }

    public byte[] getHash() throws EncryptException {
        try {
            MessageDigest md = MessageDigest.getInstance(digest);
            md.update(salt);

            return md.digest(key.getEncoded());
        } catch (NoSuchAlgorithmException e) {
            throw new EncryptException(e.getMessage());
    } }

    byte[] getEncoded() {
        return key.getEncoded();
} }
