package com.encrypt.midford.ki.recyclermanager

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION

import com.encrypt.midford.ki.R
import com.encrypt.midford.ki.utils.Utils

import java.util.ArrayList
import java.util.Collections
import java.util.Comparator

class RecyclerViewManager<T : ListItem> (private val context: Context)
    : OnRefreshListener, ListItemAdapter.ViewHolderOnClickListener<T> {

    private var listener: OnViewStateChangeListener? = null
    private var menuListener: OnMenuItemSelectedListener<T>? = null
    private var clickListener: OnListItemClickListener<T>? = null
    private var mComparator: Comparator<T>? = null

    val view: View
    private val listAdapter: ListItemAdapter<T>
    private val listRecycler: RecyclerView
    private val listTracker: SelectionTracker<Long>
    private val swipeRefresh: SwipeRefreshLayout
    private val loadingView: ProgressBar
    private val emptyTextView: TextView

    private var selectionEnabled = false
    private var dataSet: List<T>? = null

    var isLoading: Boolean = false
        set(value) {
            field = value
            refresh()
        }

    val selection: List<T>
        get() {
            return dataSet?.filter{ i -> listTracker.selection.contains(i.stableId) }
                    ?: ArrayList()
        }

    val selectionCount: Int
        get() = listTracker.selection.size()

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater.inflate(R.layout.comp_list, null)

        listRecycler = view.findViewById(R.id.recycler_list)
        emptyTextView = view.findViewById(R.id.text_empty_list)
        loadingView = view.findViewById<ProgressBar>(R.id.loading_progress)
        swipeRefresh = view.findViewById(R.id.pullToRefresh)

        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = RecyclerView.VERTICAL
        listRecycler.layoutManager = layoutManager

        swipeRefresh.setColorSchemeColors(
                  ContextCompat.getColor(context, R.color.Primary)
                , ContextCompat.getColor(context, R.color.PrimaryVariant)
                , ContextCompat.getColor(context, R.color.PrimaryDark))
        swipeRefresh.setOnRefreshListener(this)
        swipeRefresh.isEnabled = false

        dataSet = ArrayList()
        listAdapter = ListItemAdapter(this)
        listRecycler.adapter = listAdapter

        listTracker = SelectionTracker.Builder(
                      "list-item-selection"
                    , listRecycler
                    , ListItemKeyProvider(listRecycler)
                    , ListItemDetailsLookup(listRecycler)
                    , StorageStrategy.createLongStorage())
                .withSelectionPredicate(
                    ListItemSelectionPredicate()
                )
                .build()

        listTracker.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                if (listener != null)
                    listener?.onStateChange()
                }
            } )

        listAdapter.setTracker(listTracker)

        refresh()
    }

    fun setItems(items: List<T>) {
        applySort(items)
        dataSet = items
        listAdapter.submitList(items)
        refresh()
    }

    fun setComparator(comparator: Comparator<T>) {
        this.mComparator = comparator
    }

    internal fun setEmptyStateText(r: Int) {
        emptyTextView.setText(r)
    }

    internal fun setOnStateChangeListener(l: OnViewStateChangeListener) {
        listener = l
    }

    internal fun setOnMenuItemSelectedListener(l: OnMenuItemSelectedListener<T>) {
        menuListener = l
    }

    internal fun setOnListItemClickedListener(l: OnListItemClickListener<T>) {
        clickListener = l
    }

    internal fun withBottomPadding() {
        listRecycler.setPadding(0, 0, 0, Utils.convertDpToPx(context,
                context.resources.getDimension(R.dimen.list_view_bottom_pad)))
        listRecycler.clipToPadding = false
    }

    internal fun enableSwipeToRefresh() {
        swipeRefresh.isEnabled = true
    }

    internal fun enableSelectionMode() {
        selectionEnabled = true
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) return

        listTracker.onRestoreInstanceState(savedInstanceState)
    }

    fun onSaveInstanceState(outState: Bundle) {
        listTracker.onSaveInstanceState(outState)
    }

    override fun onRefresh() {
        refresh()
        swipeRefresh.isRefreshing = false
    }

    override fun onClick(i: T, v: View) {
        if (!hasSelection() && v.id == R.id.list_item_sub_menu) {
            onMenuSelect(i, v)
            return
        }

        if (clickListener != null) {
            clickListener?.onListItemClicked(v, i)
    } }

    private fun onMenuSelect(item: T, v: View) {
        val popup = PopupMenu(context, v)
        popup.menuInflater.inflate(item.menuResource, popup.menu)
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { m ->
            if (menuListener != null) {
                menuListener?.onMenuItemSelected(m, item)
                true
            } else {
                false
            }
        } )
        popup.show()
    }

    private fun refresh() {
        if (isLoading) {
            emptyTextView.visibility = GONE
            loadingView.visibility = VISIBLE
            listRecycler.visibility = GONE
            return
        }

        if (dataSet == null || dataSet!!.isEmpty()) {
            emptyTextView.visibility = VISIBLE
            loadingView.visibility = GONE
            listRecycler.visibility = GONE
            return
        }

        emptyTextView.visibility = GONE
        loadingView.visibility = GONE
        listRecycler.visibility = VISIBLE
    }

    private fun applySort(items: List<T>) {
        Collections.sort(items, mComparator)
    }

    fun clearSelection() {
        listTracker.clearSelection()
    }

    fun hasSelection(): Boolean {
        return listTracker.hasSelection()
    }

    fun selectAll() {
        listTracker.setItemsSelected(
                dataSet?.map{ it.stableId } ?: ArrayList(), true)
    }

    interface OnViewStateChangeListener {

        fun onStateChange()
    }

    interface OnMenuItemSelectedListener<T> {

        fun onMenuItemSelected(m: MenuItem, i: T)
    }

    interface OnListItemClickListener<T> {

        fun onListItemClicked(v: View, i: T)
    }

    internal inner class ListItemKeyProvider(private val recyclerView: RecyclerView)
        : ItemKeyProvider<Long>(SCOPE_MAPPED) {

        override fun getKey(pos: Int): Long? {
            return recyclerView.adapter?.getItemId(pos)
        }

        override fun getPosition(key: Long): Int {
            return recyclerView.findViewHolderForItemId(key)?.layoutPosition ?: NO_POSITION
        }
    }

    internal inner class ListItemSelectionPredicate
        : SelectionTracker.SelectionPredicate<Long>() {
        override fun canSetStateForKey(key: Long, nextState: Boolean): Boolean {
            return selectionEnabled
        }

        override fun canSetStateAtPosition(pos: Int, nextState: Boolean): Boolean {
            return selectionEnabled
        }

        override fun canSelectMultiple(): Boolean {
            return true
        }
    }

    internal inner class ListItemDetailsLookup(private val mRecyclerView: RecyclerView)
        : ItemDetailsLookup<Long>() {

        override fun getItemDetails(e: MotionEvent): ItemDetails<Long>? {
            val view = mRecyclerView.findChildViewUnder(e.x, e.y) ?: return null

            val vh = mRecyclerView.getChildViewHolder(view)
            return (vh as ListItemAdapter.ListItemViewHolder<*>).getItemDetails()
        }
    }
}
