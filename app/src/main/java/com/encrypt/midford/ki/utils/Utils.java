package com.encrypt.midford.ki.utils;

import android.content.Context;

import com.encrypt.midford.ki.R;

import java.security.SecureRandom;
import java.util.Arrays;

import static com.encrypt.midford.ki.utils.FileType.ARCHIVE;
import static com.encrypt.midford.ki.utils.FileType.AUDIO;
import static com.encrypt.midford.ki.utils.FileType.DOC;
import static com.encrypt.midford.ki.utils.FileType.GENERIC;
import static com.encrypt.midford.ki.utils.FileType.IMAGE;
import static com.encrypt.midford.ki.utils.FileType.PDF;
import static com.encrypt.midford.ki.utils.FileType.VIDEO;

public class Utils {

    private static FileType getFileType(String ext) {
        if (ext == null)
            return GENERIC;

        if (Arrays.asList(FileType.IMAGE_FORMATS)
                .contains(ext.toLowerCase())) {

            return IMAGE;
        } else if (Arrays.asList(FileType.DOC_FORMATS)
                .contains(ext.toLowerCase())) {

            return DOC;
        } else if (Arrays.asList(FileType.VIDEO_FORMATS)
                .contains(ext.toLowerCase())) {

            return VIDEO;
        } else if (Arrays.asList(FileType.AUDIO_FORMATS)
                .contains(ext.toLowerCase())) {

            return AUDIO;
        } else if (Arrays.asList(FileType.ARCHIVE_FORMATS)
                .contains(ext.toLowerCase())) {

            return ARCHIVE;
        } else if (Arrays.asList(FileType.PDF_FORMATS)
                .contains(ext.toLowerCase())) {

            return PDF;
        } else {

            return GENERIC;
    } }

    public static int getDrawableResFromType(FileType t) {
        switch (t) {
            case AUDIO:
                return R.drawable.ic_baseline_music_note_24px;
            case VIDEO:
                return R.drawable.ic_baseline_local_movies_24px;
            case DOC:
                return R.drawable.ic_baseline_document_box_24px;
            case IMAGE:
                return R.drawable.ic_baseline_photo_24px;
            case DIRECTORY:
                return R.drawable.ic_outline_folder_24px;
            case ARCHIVE:
                return R.drawable.ic_baseline_zip_box_24px;
            case PDF:
                return R.drawable.ic_baseline_pdf_24px;
            case GENERIC:
            default:
                return R.drawable.ic_baseline_generic_file_24px;
        }
    }

    private static String getExtensionFromName(String name) {
        return ! name.contains(".") ? null
                : name.substring( name.lastIndexOf(".") )
                .replace(".", "");
    }

    public static FileType getFileTypeFromName(String name) {
        return getFileType(getExtensionFromName(name));
    }

    public static boolean isSubstring(String s0, String s1) {
        return s0 != null && s1 != null && s0.toLowerCase().contains(s1.toLowerCase());
    }

    public static byte[] generateSaltBytes() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[8];
        random.nextBytes(bytes);
        return bytes;
    }

    public static int convertDpToPx(Context c, float dp) {
        final float scale = c.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
} }
