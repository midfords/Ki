package com.encrypt.midford.ki

import android.app.Application
import androidx.lifecycle.*
import com.encrypt.midford.ki.data.ManifestRepository
import com.encrypt.midford.ki.encrypt.Key
import com.encrypt.midford.ki.room.Manifest
import com.encrypt.midford.ki.room.Profile
import com.encrypt.midford.ki.utils.Utils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

internal class SetupViewModel(
        app: Application
) : AndroidViewModel(app) {

    private val mRepository = ManifestRepository(app)

    private val phrase = MutableLiveData<String>()

    val strength: LiveData<Strength>
            = Transformations.map(phrase) { s -> calcStrength(s) }
    val status = MutableLiveData<SetupBundle>()

    // Methods

    private fun containsSymboles(s: String): Boolean
        = s.matches("(.*[~!@#\$%^&*()\\-_=+<>,.?/;:]+.*)".toRegex())

    private fun containsUpper(s: String): Boolean
        = s.matches("(.*[A-Z]+.*)".toRegex())

    private fun containsLower(s: String): Boolean
        = s.matches("(.*[a-z]+.*)".toRegex())

    private fun containsNumbers(s: String): Boolean
        = s.matches("(.*\\d+.*)".toRegex())

    private fun getRank(s: String): Int {
        var rank = 0
        if (containsSymboles(s)) rank++
        if (containsUpper(s)) rank++
        if (containsLower(s)) rank++
        if (containsNumbers(s)) rank++
        return rank
    }

    private fun passwordLength(s: String): Int
        = s.length

    private fun strongPassword(s: String): Boolean
        = passwordLength(s) > 12 || (passwordLength(s) > 7 && getRank(s) >= 3)

    private fun medPassword(s: String): Boolean
        = passwordLength(s) > 9 || ( passwordLength(s) > 7 && getRank(s) >= 2)

    private fun calcStrength(phrase: String): Strength {
        return when {
            strongPassword(phrase) -> Strength.STRONG
            medPassword(phrase) -> Strength.MEDIUM
            else -> Strength.WEAK
        }
    }

    fun setup(phrase: String, confirmation: String) {
        status.value = SetupBundle(SetupBundle.Status.LOADING)

        GlobalScope.launch {
            try {
                if (phrase.isEmpty())
                    throw Exception("Enter password")

                if (confirmation.isEmpty())
                    throw Exception("Confirm password")

                if (phrase != confirmation)
                    throw Exception("Passwords do not match")

                val length = phrase.length
                val phraseBytes = CharArray(length)
                phrase.toCharArray(phraseBytes, 0, 0, length)

                val salt = Utils.generateSaltBytes()
                val key = Key(salt, phraseBytes, "PBKDF2withHmacSHA1", "sha-512")

                var profile = Profile(
                        0, key.hash, salt, "AES", "sha-512", "PBKDF2withHmacSHA1")

                val id = mRepository.insertProfile(profile)

                profile = mRepository.getProfile(id)
                        ?: throw IllegalStateException("Default profile could not be found.")

                val manifest = Manifest(id)
                mRepository.insertManifest(manifest)

                status.postValue(SetupBundle(SetupBundle.Status.SUCCEEDED, key, profile))
            } catch (e: Exception) {

                status.postValue(SetupBundle(SetupBundle.Status.FAILED, e))
            }
        }
    }

    fun setPhrase(phrase: String) {
        this.phrase.value = phrase
    }

    // Classes

    enum class Strength {
        WEAK, MEDIUM, STRONG
    }

    internal class SetupBundle {
        var status: Status
            private set
        var exception: Exception? = null
            private set
        var key: Key? = null
            private set
        var profile: Profile? = null
            private set

        constructor(status: Status) {
            this.status = status
            this.key = null
            this.profile = null
            this.exception = null
        }

        constructor(status: Status, key: Key, profile: Profile) {
            this.status = status
            this.key = key
            this.profile = profile
            this.exception = null
        }

        constructor(status: Status, exception: Exception) {
            this.status = status
            this.exception = exception
        }

        enum class Status {
            LOADING, SUCCEEDED, FAILED
        }
    }
}
