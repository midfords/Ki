package com.encrypt.midford.ki.data

import android.app.Application
import com.encrypt.midford.ki.room.*

class ManifestRepository constructor(
        application: Application
) {

    private val manifestDao: ManifestDao
    private val profileDao: ProfileDao

    init {
        val database: AppDatabase = AppDatabase.getManifestDatabase(
                application.applicationContext
        )!!
        manifestDao = database.manifestDao()
        profileDao = database.profileDao()
    }

    fun getManifest()
        = manifestDao.getManifest()

    fun doesManifestExist(): Boolean
        = manifestDao.getManifest() != null

    fun insertManifest(manifest: Manifest)
        = manifestDao.insertManifest(manifest)

    fun getProfile(id: Long)
        = profileDao.getProfile(id)

    fun insertProfile(profile: Profile): Long {
        return profileDao.insertProfile(profile)
    }

    companion object {

        @Volatile private var instance: ManifestRepository? = null

        fun getInstance(application: Application) =
                instance ?: synchronized(this) {
                    instance
                            ?: ManifestRepository(application).also {
                        instance = it
                    }
                }
    }
}
