package com.encrypt.midford.ki.encrypt;

public class EncryptException extends Exception {

    EncryptException(String message) {
        super(message);
} }
