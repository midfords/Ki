package com.encrypt.midford.ki.room

import androidx.annotation.NonNull
import androidx.room.*
import com.encrypt.midford.ki.utils.Converter
import java.util.*

@Entity(
        tableName = "folders"
        , indices = [
            Index(name = "folder_parent_index", value = arrayOf("parent"))
          , Index(name = "folder_profile_index", value = arrayOf("profile"))
          ]
      , foreignKeys = [
          ForeignKey(
              entity = EncryptedFolder::class
            , parentColumns = arrayOf("id")
            , childColumns = arrayOf("parent")
          )
        , ForeignKey(
              entity = Profile::class
            , parentColumns = arrayOf("id")
            , childColumns = arrayOf("profile")
) ] )

data class EncryptedFolder(
        @PrimaryKey(autoGenerate = true) @NonNull
        @ColumnInfo(name = "id")
        var Id: Long,

        @ColumnInfo(name = "parent")
        var ParentId: Long?,

        @ColumnInfo(name = "name", typeAffinity = ColumnInfo.BLOB)
        var Name: ByteArray,

        @TypeConverters(Converter::class)
        @ColumnInfo(name = "created")
        var Created: Date,

        @ColumnInfo(name = "profile")
        var ProfileId: Long
) {

    override fun equals(other: Any?): Boolean {
        return this === other
                || ( javaClass == other?.javaClass
                      && Id == (other as EncryptedFolder).Id )
    }

    override fun hashCode(): Int {
        return Id.hashCode()
    }
}
