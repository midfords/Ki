package com.encrypt.midford.ki.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesModel {

    private static SharedPreferences sp;

    // Constructor

    public SharedPreferencesModel(Context c) {
        sp = PreferenceManager.getDefaultSharedPreferences(c);
    }

    // Preferences

    public static final class SortOrders {
        public static final String TAG = "pref_sort_order_list";
        public static final String ASCENDING = "0";
        public static final String DESCENDING = "1";
    }

    public static final class DirectoryOrders {
        public static final String TAG = "pref_sort_dirs_list";
        public static final String FIRST = "0";
        public static final String LAST = "1";
        public static final String NONE = "2";
    }

    public static final class Themes {
        public static final String TAG = "pref_title_theme";
        public static final String LIGHT = "0";
        public static final String DARK = "1";
    }

    public String getSortOrder() {
        return sp.getString(SortOrders.TAG, SortOrders.ASCENDING);
    }

    public String getDirectoriesOrder() {
        return sp.getString(DirectoryOrders.TAG, DirectoryOrders.FIRST);
    }

    public void setSortOrder(String o) {
        sp.edit().putString(SortOrders.TAG, o).apply();
    }

    public void setDirectoriesOrder(String o) {
        sp.edit().putString(DirectoryOrders.TAG, o).apply();
    }

    public static final class Algorithms {
        public static final String AES = "aes";
    }
}
