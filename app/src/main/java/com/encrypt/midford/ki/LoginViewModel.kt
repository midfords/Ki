package com.encrypt.midford.ki

import android.app.Application
import androidx.lifecycle.*
import com.encrypt.midford.ki.data.ManifestRepository
import com.encrypt.midford.ki.encrypt.Key
import com.encrypt.midford.ki.room.Profile
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

internal class LoginViewModel(
        app: Application
) : AndroidViewModel(app) {

    private val mRepository = ManifestRepository(app)

    val status = MutableLiveData<LoginBundle>()

    // Methods

    fun login(phrase: String) {
        status.value = LoginBundle(LoginBundle.Status.LOADING)

        GlobalScope.launch {
            try {
                if (phrase.isEmpty())
                    throw Exception("Enter password.")

                val length = phrase.length
                val phraseBytes = CharArray(length)
                phrase.toCharArray(phraseBytes, 0, 0, length)

                val manifest = mRepository.getManifest()
                        ?: throw IllegalStateException("App manifest could not be found.")

                val profile = mRepository.getProfile(manifest.DefaultProfileId)
                        ?: throw IllegalStateException("Default profile could not be found.")

                val key = Key(
                        profile.Salt, phraseBytes, profile.KeyStore, profile.Digest)

                if (!Arrays.equals(profile.Hash, key.hash))
                    throw Exception("Incorrect password.")

                status.postValue(LoginBundle(LoginBundle.Status.SUCCEEDED, key, profile))
            } catch (e: Exception) {

                status.postValue(LoginBundle(LoginBundle.Status.FAILED, e))
            }
        }
    }

    // Classes

    internal class LoginBundle {
        var status: Status
            private set
        var exception: Exception? = null
            private set
        var key: Key? = null
            private set
        var profile: Profile? = null
            private set

        constructor(status: Status) {
            this.status = status
            this.key = null
            this.profile = null
            this.exception = null
        }

        constructor(status: Status, key: Key, profile: Profile) {
            this.status = status
            this.key = key
            this.profile = profile
            this.exception = null
        }

        constructor(status: Status, exception: Exception) {
            this.status = status
            this.exception = exception
        }

        enum class Status {
            LOADING, SUCCEEDED, FAILED
        }
    }
}
