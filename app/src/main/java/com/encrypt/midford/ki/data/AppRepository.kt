package com.encrypt.midford.ki.data

import android.app.Application
import android.content.ContentResolver
import android.net.Uri
import android.provider.OpenableColumns
import androidx.lifecycle.LiveData
import com.encrypt.midford.ki.encrypt.Encryptor
import com.encrypt.midford.ki.room.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.InputStream
import java.util.*

class AppRepository constructor(
          application: Application
        , private val mEncryptor: Encryptor
){

    private val storageModel: EncryptedStorageModel
            = EncryptedStorageModel.getInstance(mEncryptor)

    private val encryptedFileDao: EncryptedFileDao
    private val encryptedFolderDao: EncryptedFolderDao

    init {
        val database: AppDatabase = AppDatabase.getDatabase(
                application.applicationContext
        )!!

        encryptedFileDao = database.fileDao()
        encryptedFolderDao = database.folderDao()
    }

    fun getEncryptedFile(id: Long): LiveData<EncryptedFile?>
            = encryptedFileDao.getFile(id)

    fun getEncryptedFolder(id: Long?): LiveData<EncryptedFolder?>
            = encryptedFolderDao.getFolder(id)

    fun getEncryptedFiles(folder: EncryptedFolder?): LiveData<List<EncryptedFile>?> {
        return if (folder == null)
                encryptedFileDao.getRootFiles()
            else
                encryptedFileDao.getFiles(folder.Id)
    }

    fun getEncryptedFolders(folder: EncryptedFolder?): LiveData<List<EncryptedFolder>?> {
        return if (folder == null)
                encryptedFolderDao.getRootFolders()
            else
                encryptedFolderDao.getFolders(folder.Id)
    }

    fun getAllEncryptedFiles(): LiveData<List<EncryptedFile>?> {
        return encryptedFileDao.getAllFiles();
    }

    fun getAllEncryptedFolders(): LiveData<List<EncryptedFolder>?> {
        return encryptedFolderDao.getAllFolders()
    }

    // Import

    private fun getNameFromUri(uri: Uri, contentResolver: ContentResolver): String {
        var name = ""
        contentResolver.query(uri, null, null
                , null, null)?.use {

            val index = it.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            it.moveToFirst()
            name = it.getString(index)
        }

        return name
    }

    private fun getStreamFromUri(uri: Uri, contentResolver: ContentResolver): InputStream? {
        return contentResolver.openInputStream(uri)
    }

    fun addEncryptedFiles(uris: List<Uri>, parent: EncryptedFolder?
                          , deleteOnComplete: Boolean, contentResolver: ContentResolver) {
        uris.forEach {
            try {
                val stream = getStreamFromUri(it, contentResolver)
                val name = getNameFromUri(it, contentResolver)

                if (stream != null)
                    addEncryptedFile(stream, it, name.toByteArray(), parent
                            , deleteOnComplete, contentResolver)
            } catch (ignored: Exception) {
                println("Import failed.\n$it \n$contentResolver")
            }
        }
    }

    private fun addEncryptedFile(stream: InputStream, uri: Uri, name: ByteArray
                                 , parent: EncryptedFolder?, deleteOnComplete: Boolean
                                 , contentResolver: ContentResolver) {
        GlobalScope.launch {
            val pair = storageModel.writeFile(stream)
            val d = Calendar.getInstance().time
            val n = mEncryptor.encrypt(name)

            if (pair.first) {
                val f = EncryptedFile(
                        0, parent?.Id, n, pair.second, d, mEncryptor.profile.Id)
                encryptedFileDao.insertFile(f)

//                if (deleteOnComplete) {
//                    storageModel.deleteFileFromUri(uri, contentResolver)
//                }
            }
        }
    }

    fun addEncryptedFolder(name: ByteArray, parent: EncryptedFolder?) {
        GlobalScope.launch {
            val d = Calendar.getInstance().time
            val n = mEncryptor.encrypt(name)
            val f = EncryptedFolder(
                    0, parent?.Id, n, d, mEncryptor.profile.Id)
            encryptedFolderDao.insertFolder(f)
        }
    }

    fun readEncryptedFile(id: String): InputStream? {
        return storageModel.readFile(id)
    }

    // Export

    private fun exportEncryptedFileSync(file: EncryptedFile, path: File
                                        , deleteOnComplete: Boolean): Boolean {
        val result = storageModel.exportFile(file, path)

        if (result && deleteOnComplete)
            deleteEncryptedFileSync(file)

        return result
    }

    private fun exportEncryptedFolderSync(folder: EncryptedFolder, path: File
                                          , deleteOnComplete: Boolean): Boolean {
        val pair = storageModel.exportFolder(folder, path)
        var result = pair.first

        if (result) {
            result = result && exportEncryptedFoldersSync(
                          encryptedFolderDao.getFoldersSync(folder.Id)
                        , File(path, pair.second), deleteOnComplete)
            result = result && exportEncryptedFilesSync(
                          encryptedFileDao.getFilesSync(folder.Id)
                        , File(path, pair.second), deleteOnComplete)

            if (deleteOnComplete)
                deleteEncryptedFolderSync(folder)
        }
        return result
    }

    private fun exportEncryptedFilesSync(files: List<EncryptedFile>?, path: File
                , deleteOnComplete: Boolean): Boolean {
        var result = true
        files?.forEach {
            result = result && exportEncryptedFileSync(it, path, deleteOnComplete)
        }
        return result
    }

    private fun exportEncryptedFoldersSync(folders: List<EncryptedFolder>?, path: File
                , deleteOnComplete: Boolean): Boolean {
        var result = true
        folders?.forEach {
            result = result && exportEncryptedFolderSync(it, path, deleteOnComplete)
        }
        return result
    }

    fun exportEncryptedFile(file: EncryptedFile, root: String, deleteOnComplete: Boolean) {
        GlobalScope.launch {
            exportEncryptedFileSync(file, File(root), deleteOnComplete)
        }
    }

    fun exportEncryptedFolder(folder: EncryptedFolder, root: String, deleteOnComplete: Boolean) {
        GlobalScope.launch {
            exportEncryptedFolderSync(folder, File(root), deleteOnComplete)
        }
    }

    fun exportEncryptedFiles(files: List<EncryptedFile>, path: String, deleteOnComplete: Boolean) {
        GlobalScope.launch {
            exportEncryptedFilesSync(files, File(path), deleteOnComplete)
        }
    }

    fun exportEncryptedFolders(folders: List<EncryptedFolder>, path: String, deleteOnComplete: Boolean) {
        GlobalScope.launch {
            exportEncryptedFoldersSync(folders, File(path), deleteOnComplete)
        }
    }

    // Rename

    fun renameEncryptedFile(file: EncryptedFile, name: ByteArray) {
        GlobalScope.launch {
            file.Name = mEncryptor.encrypt(name)
            encryptedFileDao.updateFile(file)
        }
    }

    fun renameEncryptedFolder(folder: EncryptedFolder, name: ByteArray) {
        GlobalScope.launch {
            folder.Name = mEncryptor.encrypt(name)
            encryptedFolderDao.updateFolder(folder)
        }
    }

    // Move

    private fun isValidMove(folder: EncryptedFolder, parent: EncryptedFolder?): Boolean {
        if (parent == null)
            return true
        if (folder.Id == parent.Id)
            return false

        return isValidMove(folder, encryptedFolderDao.getFolderSync(parent.ParentId))
    }

    fun moveEncryptedFile(file: EncryptedFile, parent: EncryptedFolder?) {
        GlobalScope.launch {
            file.ParentId = parent?.Id
            encryptedFileDao.updateFile(file)
        }
    }

    fun moveEncryptedFolder(folder: EncryptedFolder, parent: EncryptedFolder?) {
        GlobalScope.launch {
            val result = isValidMove(folder, parent);

            if (result) {
                folder.ParentId = parent?.Id
                encryptedFolderDao.updateFolder(folder)
            }
        }
    }

    // Delete

    private fun deleteEncryptedFileSync(file: EncryptedFile) {
        storageModel.deleteFile(file.FilesystemId)
        encryptedFileDao.deleteFile(file)
    }

    private fun deleteEncryptedFolderSync(folder: EncryptedFolder) {
        deleteEncryptedFilesSync(encryptedFileDao.getFilesSync(folder.Id))
        deleteEncryptedFoldersSync(encryptedFolderDao.getFoldersSync(folder.Id))
        encryptedFolderDao.deleteFolder(folder)
    }

    private fun deleteEncryptedFilesSync(files: List<EncryptedFile>?) {
        files?.forEach {
            deleteEncryptedFileSync(it)
        }
    }

    private fun deleteEncryptedFoldersSync(folders: List<EncryptedFolder>?) {
        folders?.forEach {
            deleteEncryptedFolderSync(it)
        }
    }

    fun deleteEncryptedFile(file: EncryptedFile) {
        GlobalScope.launch {
            deleteEncryptedFileSync(file)
        }
    }

    fun deleteEncryptedFolder(folder: EncryptedFolder) {
        GlobalScope.launch {
            deleteEncryptedFolderSync(folder)
        }
    }

    fun deleteEncryptedFiles(files: List<EncryptedFile>) {
        GlobalScope.launch {
            deleteEncryptedFilesSync(files)
        }
    }

    fun deleteEncryptedFolders(folders: List<EncryptedFolder>) {
        GlobalScope.launch {
            deleteEncryptedFoldersSync(folders)
        }
    }

    companion object {

        @Volatile private var instance: AppRepository? = null

        fun getInstance(application: Application, encryptor: Encryptor) =
                instance ?: synchronized(this) {
                    instance
                            ?: AppRepository(application, encryptor).also {
                        instance = it
                    }
                }
    }
}
