package com.encrypt.midford.ki;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.encrypt.midford.ki.encrypt.Key;
import com.encrypt.midford.ki.room.Profile;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private static final String PASS_TAG = "PASS";

    private AppCompatActivity mActivity;
    private LoginViewModel mViewModel;

    private ProgressBar progress;
    private ImageView login;
    private EditText pass;

    public LoginFragment() { /* Required empty public constructor */ }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_frag, container, false);

        pass = view.findViewById(R.id.password_et);
        progress = view.findViewById(R.id.login_progress);
        login = view.findViewById(R.id.login_action);

        login.setOnClickListener(this);

        if (savedInstanceState != null) {
            pass.setText(savedInstanceState.getString(PASS_TAG));
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        mViewModel.getStatus().observe(getViewLifecycleOwner()
                , new Observer<LoginViewModel.LoginBundle>() {
                    @Override
                    public void onChanged(LoginViewModel.LoginBundle bundle) {
                        LoginViewModel.LoginBundle.Status code = bundle.getStatus();

                        switch(code) {
                            case LOADING:

                                progress.setVisibility(View.VISIBLE);
                                pass.setEnabled(false);
                                login.setEnabled(false);
                                break;
                            case SUCCEEDED:
                                pass.setText("");

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.putExtra(Key.class.getName(), bundle.getKey());
                                intent.putExtra(Profile.class.getName(), bundle.getProfile());
                                startActivity(intent);

                                if (mActivity != null)
                                    mActivity.finish();
                                break;
                            case FAILED:
                                if (bundle.getException() != null) {
                                    pass.setError(bundle.getException().getMessage());
                                    Log.e(LoginFragment.class.getName(), bundle.getException().getMessage());
                                }
                            default:
                                progress.setVisibility(View.INVISIBLE);
                                pass.setEnabled(true);
                                login.setEnabled(true);
                        }
        } } );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(PASS_TAG, pass.getText().toString());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onClick(View v) {
        mViewModel.login(pass.getText().toString());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
