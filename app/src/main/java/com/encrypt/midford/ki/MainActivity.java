package com.encrypt.midford.ki;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.encrypt.midford.ki.components.CustomDialogueBuilder;
import com.encrypt.midford.ki.data.SharedPreferencesModel;
import com.encrypt.midford.ki.encrypt.Encryptor;
import com.encrypt.midford.ki.encrypt.Key;
import com.encrypt.midford.ki.recyclermanager.RecyclerViewManager;
import com.encrypt.midford.ki.recyclermanager.RecyclerViewManagerBuilder;
import com.encrypt.midford.ki.room.Profile;
import com.encrypt.midford.ki.utils.Paths;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
        , PopupMenu.OnMenuItemClickListener
        , RecyclerViewManager.OnViewStateChangeListener
        , RecyclerViewManager.OnMenuItemSelectedListener<MainViewModelListItem>
        , RecyclerViewManager.OnListItemClickListener<MainViewModelListItem> {

    private MainViewModel mViewModel;
    private RecyclerViewManager<MainViewModelListItem> mListManager;
    private Encryptor mEncryptor;

    private LinearLayout mToolbarMain;
    private LinearLayout mToolbarSelect;
    private LinearLayout mToolbarSearch;
    private TextView mToolbarSelectText;
    private EditText mToolbarSearchEditText;
    private LinearLayout mSubToolbar;
    private TextView mSubToolbarText;

    // Setup

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Key mKey = getIntent().getParcelableExtra(Key.class.getName());
        Profile profile = getIntent().getParcelableExtra(Profile.class.getName());
        mEncryptor = new Encryptor(mKey, profile);

        mToolbarMain = findViewById(R.id.main_toolbar);
        mToolbarSelect = findViewById(R.id.select_toolbar);
        mToolbarSelectText = findViewById(R.id.tv_select_title);
        mToolbarSearch = findViewById(R.id.search_toolbar);
        mToolbarSearchEditText = findViewById(R.id.et_search);
        final ImageView menu = findViewById(R.id.list_item_sub_menu);
        final ImageView back = findViewById(R.id.iv_back_arrow);
        final LinearLayout search = findViewById(R.id.ll_search);
        menu.setOnClickListener(this);
        back.setOnClickListener(this);
        search.setOnClickListener(this);
        final ImageView selectMenu = findViewById(R.id.select_menu);
        final ImageView selectCancel = findViewById(R.id.iv_cancel);
        selectMenu.setOnClickListener(this);
        selectCancel.setOnClickListener(this);
        final ImageView searchClear = findViewById(R.id.search_clear);
        final ImageView searchBack = findViewById(R.id.search_back);
        searchClear.setOnClickListener(this);
        searchBack.setOnClickListener(this);
        mSubToolbar = findViewById(R.id.main_sub_toolbar);
        mSubToolbarText = findViewById(R.id.sub_toolbar_text);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent picker = new Intent();
                picker.setAction(Intent.ACTION_GET_CONTENT);
                picker.setType("*/*");
                picker.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                picker.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(picker, 0);
        } } );

        mViewModel = ViewModelProviders.of(this
                , new MainViewModelFactory(getApplication(), mEncryptor))
                .get(MainViewModel.class);

        mViewModel.getResults().observe(this, new Observer<List<MainViewModelListItem>>() {
            @Override
            public void onChanged(List<MainViewModelListItem> items) {
                if (mListManager != null)
                    mListManager.setItems(items);
        } } );

        mViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean flag) {
                if (mListManager != null)
                    mListManager.setLoading(flag);
            }
        });

        mViewModel.isRootFolder().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean b) {
                back.setVisibility(b ? View.INVISIBLE : View.VISIBLE);
        } } );

        mViewModel.getCurFolderName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String name) {
                if (mSubToolbarText == null)
                    return;

                mSubToolbar.setVisibility(View.VISIBLE);
                mSubToolbarText.setText(name == null
                        ? getResources().getString(R.string.home)
                        : name);
        } } );

        mToolbarSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mViewModel.setSearchQuery(s.toString());
        } } );

        if (mListManager == null) {

            final SharedPreferencesModel settings = new SharedPreferencesModel(this);
            RecyclerViewManagerBuilder<MainViewModelListItem> builder =
                    new RecyclerViewManagerBuilder<>(this);

            mListManager = builder
                    .enableSwipeToRefresh()
                    .enableSelectionMode()
                    .withBottomPadding()
                    .startWithLoading()
                    .setEmptyStateText(R.string.empty_list_message)
                    .setComparator(new Comparator<MainViewModelListItem>() {
                        @Override
                        public int compare(MainViewModelListItem obj1, MainViewModelListItem obj2) {

                            String sort = settings.getSortOrder();
                            String order = settings.getDirectoriesOrder();

                            if (SharedPreferencesModel.DirectoryOrders.FIRST.equals(order)
                                    && (obj1.isDirectory() ^ obj2.isDirectory())) {
                                return obj1.isDirectory() ? -1 : 1;
                            }

                            if (SharedPreferencesModel.DirectoryOrders.LAST.equals(order)
                                    && (obj1.isDirectory() ^ obj2.isDirectory())) {
                                return obj1.isDirectory() ? 1 : -1;
                            }

                            if (SharedPreferencesModel.SortOrders.ASCENDING.equals(sort)) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }

                            if (SharedPreferencesModel.SortOrders.DESCENDING.equals(sort)) {
                                return obj2.getName().compareToIgnoreCase(obj1.getName());
                            }

                            return 0;
                    } } )
                    .setOnStateChangeListener(this)
                    .setOnMenuItemSelectedListener(this)
                    .setOnListItemClickListener(this)
                    .getManager();

        }

        RelativeLayout containerLayout = findViewById(R.id.fragment_container);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        View mainView = mListManager.getView();
        containerLayout.addView(mainView, params);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null)
            return;

        mListManager.onRestoreInstanceState(savedInstanceState);
        String query = savedInstanceState.getString("query");
        mViewModel.setSearchQuery(query);

        if (query != null && !query.isEmpty())
            mToolbarSearchEditText.setText(query);

        onStateChange();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("query", mViewModel.getSearchQuery());
        mListManager.onSaveInstanceState(outState);
    }

    private int show(boolean flag) {
        return flag ? View.VISIBLE : View.GONE;
    }

    private void showKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null)
            inputMethodManager.showSoftInput(
                    mToolbarSearchEditText, 0);
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(
                    mToolbarSearchEditText.getWindowToken(), 0);
    }

    @Override
    public void onStateChange() {
        mToolbarMain.setVisibility(
                show(!mListManager.hasSelection() && !mViewModel.isSearchMode()));
        mToolbarSearch.setVisibility(
                show(mViewModel.isSearchMode() && !mListManager.hasSelection()));
        mToolbarSelect.setVisibility(
                show(mListManager.hasSelection()));
        mSubToolbar.setVisibility(
                show(!mViewModel.isSearchMode()));

        if (mListManager.hasSelection()) {
            int i = mListManager.getSelectionCount();
            mToolbarSelectText.setText(
                    getResources().getQuantityString(R.plurals.selection_header, i, i));
            mToolbarSearchEditText.clearFocus();
            hideKeyboard();
        } else if (mViewModel.isSearchMode()) {
            mToolbarSearchEditText.requestFocus();
            showKeyboard();
        } else {
            mToolbarSearchEditText.clearFocus();
            hideKeyboard();
        }
    }

    private int filterDups(List<Uri> uris) {

        if (uris.size() <= 1)
            return 0;

        Collections.sort(uris, new Comparator<Uri>() {
            @Override
            public int compare(Uri o1, Uri o2) {
                return o1.compareTo(o2);
            }
        });

        int count = 0;
        for(int i = 0; i < uris.size() - 1; i++) {
            if (uris.get(i).equals(uris.get(i + 1))) {
                uris.remove(i + 1);
                count ++;
            }
        }

        return count;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        try {
            List<Uri> uris = new ArrayList<>();

            if (data.getClipData() != null) {
                ClipData contents = data.getClipData();
                int contentsCount = contents != null ? contents.getItemCount() : 0;
                for (int i = 0; i < contentsCount; i++) {
                    ClipData.Item item = contents.getItemAt(i);
                    uris.add(item.getUri());
                }
            } else {
                String content = data.getDataString();
                uris.add(Uri.parse(content));
            }

            int removed = filterDups(uris);

            showImportDialogue(uris, removed);
        } catch (Exception e) {
            Log.e(MainActivity.class.getName(), e.getMessage());
    } }

    @Override
    public void onMenuItemSelected(@NonNull MenuItem m, MainViewModelListItem i) {
        switch (m.getItemId()) {
            case R.id.action_decrypt:
                showExportDialogue(i);
                break;
            case R.id.action_rename:
                showRenameDialogue(i);
                break;
            case R.id.action_move:
                showMoveDialogue(i);
                break;
            case R.id.action_delete:
                showDeleteDialogue(i);
                break;
    } }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.action_settings:
//                launchSettingsActivity();
//                return true;
            case R.id.action_lock_app:
                launchLoginActivity();
                return true;
            case R.id.action_new_folder:
                showNewFolderDialogue();
                return true;
            case R.id.action_select_all:
                mListManager.selectAll();
                break;
            case R.id.action_move:
                showMoveDialogue(mListManager.getSelection());
                return true;
            case R.id.action_decrypt:
                showExportDialogue(mListManager.getSelection());
                return true;
            case R.id.action_delete:
                showDeleteDialogue(mListManager.getSelection());
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_item_sub_menu:
                PopupMenu popup = new PopupMenu(this, v);
                popup.getMenuInflater().inflate(R.menu.main, popup.getMenu());
                popup.setOnMenuItemClickListener(this);
                popup.show();
                break;
            case R.id.iv_back_arrow:
                mViewModel.setCurFolderUp();
                break;
            case R.id.select_menu:
                PopupMenu popupSelect = new PopupMenu(this, v);
                popupSelect.getMenuInflater().inflate(R.menu.select, popupSelect.getMenu());
                popupSelect.setOnMenuItemClickListener(this);
                popupSelect.show();
                break;
            case R.id.iv_cancel:
                mListManager.clearSelection();
                break;
            case R.id.ll_search:
                mViewModel.setSearchMode(true);
                onStateChange();
                break;
            case R.id.search_clear:
                clearSearchBox();
                break;
            case R.id.search_back:
                mViewModel.setSearchMode(false);
                clearSearchBox();
                onStateChange();
                break;
    } }

    @Override
    public void onListItemClicked(@NonNull View v, MainViewModelListItem i) {
        if (i.isDirectory()) {
            mViewModel.setSearchMode(false);
            mViewModel.setCurFolder(i.getId());
            clearSearchBox();
            onStateChange();

        } else {
            Intent intent = new Intent(this, PreviewActivity.class);

            Bundle bundle = new Bundle();
            bundle.putParcelable(Encryptor.class.getName(), mEncryptor);
            bundle.putLong("Id", i.getId());

            intent.putExtras(bundle);
            startActivity(intent);
    } }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        List<MainViewModelListItem> selection = mListManager.getSelection();

        switch (item.getItemId()) {
            case R.id.action_new_folder:
                showNewFolderDialogue();
                return true;
            case R.id.action_decrypt:
                showExportDialogue(selection);
                return true;
            case R.id.action_delete:
                showDeleteDialogue(selection);
                return true;
            case R.id.action_move:
                showMoveDialogue(selection);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mListManager.hasSelection()) {
            mListManager.clearSelection();
            onStateChange();

        } else if (mViewModel.isSearchMode()) {
            clearSearchBox();
            mViewModel.setSearchMode(false);
            onStateChange();

        } else if (!(mViewModel.getCurFolder() == null)) {
            mViewModel.setCurFolderUp();
            onStateChange();

        } else
            super.onBackPressed();
    }

    private void launchSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void launchLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void clearSearchBox() {
        if (mToolbarSearchEditText != null)
            mToolbarSearchEditText.setText(null);

        mViewModel.setSearchQuery(null);
    }

    private void showMoveDialogue(MainViewModelListItem item) {
        List<MainViewModelListItem> list = new ArrayList<>();
        list.add(item);
        showMoveDialogue(list);
    }
    private void showMoveDialogue(List<MainViewModelListItem> items) {
        new MoveDialogue(this)
                .setEncryptor(mEncryptor)
                .setCurFolder(mViewModel.getCurFolder())
                .setDefaultFolderName(getResources().getString(R.string.home))
                .setItems(items)
                .setActionListener(new MoveDialogue.ActionListener() {
                        @Override
                        public void onCancel() {}

                        @Override
                        public void onSuccess() {
                            mListManager.clearSelection();
                } } )
                .show();
    }

    private void showNewFolderDialogue() {
        final EditText et = new EditText(this);
        et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        et.setHint(R.string.new_folder_hint);
        et.setText(R.string.new_folder_hint);
        et.setSelectAllOnFocus(true);

        CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_outline_create_new_folder_24px)
                .setTitle(R.string.new_folder_title)
                .addContentView(et)
                .setPrimaryActionText(R.string.ok)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        if (et.getText().toString().isEmpty()) {
                            et.setError(context.getResources().getString(R.string.no_text_err));
                            return;
                        }

                        mViewModel.newFolder(
                                et.getText().toString().getBytes(), mViewModel.getCurFolder());
                    }
                })
                .setDismissCondition(new CustomDialogueBuilder.DismissCondition() {
                    @Override
                    public boolean shouldDismiss() {
                        return !et.getText().toString().isEmpty();
                    }
                })
                .show();
    }

    private void showDeleteDialogue(final MainViewModelListItem item) {
        List<MainViewModelListItem> list = new ArrayList<>();
        list.add(item);
        showDeleteDialogue(list);
    }
    private void showDeleteDialogue(@NonNull final List<MainViewModelListItem> items) {
        if (items.isEmpty())
            return;

        CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_outline_delete_24px)
                .setTitle(R.string.delete_title)
                .setMessage(getResources().getQuantityString(
                        R.plurals.delete_msg, items.size(), items.size()))
                .setPrimaryActionText(R.string.action_delete)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        mViewModel.deleteItems(items);
                        mListManager.clearSelection();
                    }
                })
                .show();
    }

    private void showRenameDialogue(@NonNull final MainViewModelListItem item) {
        final EditText et = new EditText(this);
        et.setText(item.getName());
        et.setSelection(0, item.getName().lastIndexOf(".") == -1
                || item.isDirectory()
                    ? item.getName().length()
                    : item.getName().lastIndexOf("."));
        et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        et.setHint(R.string.rename_hint);

        CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_outline_edit_24px)
                .setTitle(R.string.rename_title)
                .addContentView(et)
                .setPrimaryActionText(R.string.ok)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        if (et.getText().toString().isEmpty()) {
                            et.setError(context.getResources().getString(R.string.no_text_err));
                            return;
                        }

                        item.rename(et.getText().toString());
                    }
                })
                .setDismissCondition(new CustomDialogueBuilder.DismissCondition() {
                    @Override
                    public boolean shouldDismiss() {
                        return !et.getText().toString().isEmpty();
                    }
                })
                .show();
    }

    private void showExportDialogue(@NonNull final MainViewModelListItem item) {
        List<MainViewModelListItem> list = new ArrayList<>();
        list.add(item);
        showExportDialogue(list);
    }
    private void showExportDialogue(@NonNull final List<MainViewModelListItem> items) {
        if (items.isEmpty())
            return;

        final CheckBox cb = new CheckBox(this);
        cb.setText(getResources().getQuantityString(
                R.plurals.delete_from_ki_msg, items.size(), items.size()));

        final String path = Paths.EXPORT_PATH;

        final LinearLayout pathLayout = (LinearLayout) getLayoutInflater()
                .inflate(R.layout.comp_dialogue_path, null);
        TextView message = pathLayout.findViewById(R.id.tv_destination_msg);
        message.setText(path);

        CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_baseline_export_24px)
                .setTitle(R.string.export_title)
                .setMessage(getResources().getQuantityString(
                        R.plurals.export_msg, items.size(), items.size()))
                .addContentView(pathLayout)
                .addContentView(cb)
                .setPrimaryActionText(R.string.export_btn)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        mViewModel.exportItems(items, path, cb.isChecked());
                        mListManager.clearSelection();
                    } } )
                .show();
    }

    private void showImportDialogue(@NonNull final List<Uri> uris, int removed) {
        if (uris.isEmpty())
            return;

//        final CheckBox cb = new CheckBox(this);
//        cb.setText(getResources().getQuantityString(
//                R.plurals.delete_originals, uris.size(), uris.size()));

        CustomDialogueBuilder builder = CustomDialogueBuilder
                .fromContext(this)
                .setIcon(R.drawable.ic_outline_import_24px)
                .setTitle(R.string.import_title);

        if (removed > 0) {
            LinearLayout warning = (LinearLayout) getLayoutInflater()
                    .inflate(R.layout.comp_dialogue_warning, null);
            TextView message = warning.findViewById(R.id.tv_warning);
            message.setText(getResources().getQuantityString(
                    R.plurals.import_warning, removed, removed));
            builder.addContentView(warning);
        }

        builder.setMessage(getResources().getQuantityString(
                        R.plurals.import_msg, uris.size(), uris.size()))
//                .addContentView(cb)
                .setPrimaryActionText(R.string.ok)
                .setSecondaryActionText(R.string.cancel)
                .setPrimaryActionListener(new CustomDialogueBuilder.Action() {
                    @Override
                    public void onAction(@NonNull Context context) {
                        mViewModel.importFiles(uris, /*cb.isChecked()*/false
                                , context.getContentResolver());
                } } )
                .show();
    }
}
