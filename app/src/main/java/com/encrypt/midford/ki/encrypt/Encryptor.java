package com.encrypt.midford.ki.encrypt;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.encrypt.midford.ki.room.Profile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor implements Parcelable {

    private static final int BUFFER_SIZE = 4096;

    private Key key;
    private Profile profile;

    // Constructor

    public Encryptor(Key key, Profile profile) {
        this.key = key;
        this.profile = profile;
    }

    private Encryptor(Parcel parcel) {
        key = parcel.readParcelable(Key.class.getClassLoader());
        profile = parcel.readParcelable(Profile.class.getClassLoader());
    }

    public static final Creator<Encryptor> CREATOR = new Creator<Encryptor>() {
        @Override
        public Encryptor createFromParcel(Parcel in) {
            return new Encryptor(in);
        }

        @Override
        public Encryptor[] newArray(int size) {
            return new Encryptor[size];
        }
    };

    // Methods

    public byte[] encrypt(byte[] bytes) throws EncryptException {
        if (bytes == null)
            return null;

        try {
            SecretKeySpec secretKey
                    = new SecretKeySpec(key.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance(profile.getCipher());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return cipher.doFinal(bytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | BadPaddingException | IllegalBlockSizeException e) {
            throw new EncryptException(e.getMessage());
    } }

    public byte[] decrypt(byte[] bytes) throws EncryptException {
        if (bytes == null)
            return null;

        try {
            SecretKeySpec secretKey
                    = new SecretKeySpec(key.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance(profile.getCipher());
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            return cipher.doFinal(bytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | BadPaddingException | IllegalBlockSizeException e) {
            throw new EncryptException(e.getMessage());
    } }

    public void encryptStream(@NonNull InputStream input, @NonNull OutputStream output)
            throws IOException, EncryptException {
        CipherOutputStream cos = null;
        byte[] buffer = new byte[BUFFER_SIZE];

        try {
            SecretKeySpec secretKey
                    = new SecretKeySpec(key.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance(profile.getCipher());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            cos = new CipherOutputStream(output, cipher);

            int i;
            while ((i = input.read(buffer)) != -1) {
                cos.write(buffer, 0, i);
            }

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new EncryptException(e.getMessage());
        } finally {
            input.close();

            if (cos != null) {
                cos.flush();
                cos.close();
            }

            Arrays.fill(buffer, (byte) 0);
    } }

    public void decryptStream(@NonNull InputStream input, @NonNull OutputStream output)
            throws IOException, EncryptException {
        CipherInputStream cis = null;
        byte[] buffer = new byte[BUFFER_SIZE];

        try {
            SecretKeySpec secretKey
                    = new SecretKeySpec(key.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance(profile.getCipher());
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            cis = new CipherInputStream(input, cipher);

            int i;
            while ((i = cis.read(buffer)) != -1) {
                output.write(buffer, 0, i);
            }

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new EncryptException(e.getMessage());
        } finally {
            output.flush();
            output.close();

            if (cis != null)
                cis.close();

            Arrays.fill(buffer, (byte) 0);
    } }

    public InputStream getDecryptStream(@NonNull InputStream stream)
            throws IOException, EncryptException {
        try {
            SecretKeySpec secretKey
                    = new SecretKeySpec(key.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance(profile.getCipher());
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            return new CipherInputStream(stream, cipher);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new EncryptException(e.getMessage());
    } }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(key, flags);
        dest.writeParcelable(profile, flags);
    }

    // Getters & setters

    public byte[] getHash() throws EncryptException {
        return key.getHash();
    }

    public Profile getProfile() {
        return profile;
} }
