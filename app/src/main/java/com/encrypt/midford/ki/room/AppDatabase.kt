package com.encrypt.midford.ki.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.encrypt.midford.ki.utils.Converter

@Database(
          entities = [
                EncryptedFile::class
              , EncryptedFolder::class
              , Manifest::class
              , Profile::class
          ]
        , version = 1
        , exportSchema = false
)

@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun fileDao(): EncryptedFileDao
    abstract fun folderDao(): EncryptedFolderDao
    abstract fun manifestDao(): ManifestDao
    abstract fun profileDao(): ProfileDao

    companion object {

        private const val DATABASE: String = "ki_database"

        private var mInstance: AppDatabase? = null
        private var mManifestInstance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase? {
            if (mInstance == null)
                mInstance = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, DATABASE)
                        .build()

            return mInstance
        }

        fun getManifestDatabase(context: Context): AppDatabase? {
            if (mManifestInstance == null)
                mManifestInstance = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, DATABASE)
                        .allowMainThreadQueries()
                        .build()

            return mManifestInstance
        }

    }
}
