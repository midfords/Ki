package com.encrypt.midford.ki

import com.encrypt.midford.ki.recyclermanager.ListItem
import com.encrypt.midford.ki.room.EncryptedFolder

interface MainViewModelListItem : ListItem {

    val id: Long

    val item: Any

    fun rename(name: String)

    fun move(parent: EncryptedFolder?)

    fun isDirectory(): Boolean
}
