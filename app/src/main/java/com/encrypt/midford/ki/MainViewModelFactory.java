package com.encrypt.midford.ki;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.encrypt.midford.ki.encrypt.Encryptor;

public class MainViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private Encryptor mEncryptor;

    MainViewModelFactory(Application application, Encryptor encryptor) {
        mApplication = application;
        mEncryptor = encryptor;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainViewModel(mApplication, mEncryptor);
    }
}