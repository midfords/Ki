package com.encrypt.midford.ki.utils;

import android.os.Environment;

import java.io.File;

public class Paths {
    public static final String STORAGE_PATH = new File(
            Environment.getExternalStorageDirectory().getPath(), "/Ki").getPath();
    public static final String EXPORT_PATH = new File(
            Environment.getExternalStorageDirectory().getPath(), "/Ki_export").getPath();
}
