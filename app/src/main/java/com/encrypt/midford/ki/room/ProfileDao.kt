package com.encrypt.midford.ki.room

import androidx.room.*

@Dao
interface ProfileDao {

    @Query("SELECT * FROM profiles WHERE id=:id LIMIT 1")
    fun getProfile(id: Long): Profile?

    @Insert
    fun insertProfile(profile: Profile): Long

    @Update
    fun updateProfile(profile: Profile)

    @Delete
    fun deleteProfile(profile: Profile)
}
