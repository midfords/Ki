package com.encrypt.midford.ki

import android.app.Application

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

import com.encrypt.midford.ki.encrypt.EncryptException
import com.encrypt.midford.ki.encrypt.Encryptor
import com.encrypt.midford.ki.recyclermanager.ListItem

import com.encrypt.midford.ki.data.AppRepository
import com.encrypt.midford.ki.room.EncryptedFolder
import java.io.InputStream

import java.util.ArrayList

class MoveViewModel(
          app: Application
        , private val mDecryptor: Encryptor
) : AndroidViewModel(app) {

    private val mRepository: AppRepository = AppRepository(app, mDecryptor)

    var items: List<MainViewModelListItem> = ArrayList()

    private val mCurFolderId: MutableLiveData<Long?> = MutableLiveData()
    private var mCurFolder: LiveData<EncryptedFolder?>
            = Transformations.switchMap(mCurFolderId) { id -> mRepository.getEncryptedFolder(id) }

    val isRootFolder: LiveData<Boolean>
            = Transformations.map(mCurFolder) { folder ->
        folder == null
    }

    val curFolderName: LiveData<String?>
            = Transformations.map(mCurFolder) { folder ->
        if (folder == null)
            null
        else
            String(mDecryptor.decrypt(folder.Name))
    }

    private val mFolders: LiveData<List<EncryptedFolder>?>
            = Transformations.switchMap(mCurFolder) { c -> mRepository.getEncryptedFolders(c) }

    private fun shouldSkip(i: EncryptedFolder): Boolean {
        return items.find { it.isDirectory() && it.id == i.Id } != null
    }

    val folderListItems: LiveData<List<MoveListItem>?>
            = Transformations.map(mFolders) { input ->
                val result = ArrayList<MoveListItem>()

                for (i in input!!) {
                    if (shouldSkip(i))
                        continue

                    result.add(object : MoveListItem {
                        override val id: Long = i.Id

                        override val menuResource: Int = -1

                        override val drawableRes: Int = R.drawable.ic_outline_folder_24px

                        override val stream: InputStream? = null

                        override val stableId: Long = i.Id

                        override val name: String = try {
                                String(mDecryptor.decrypt(i.Name))
                            } catch (e: EncryptException) {
                                ""
                        }

                        override fun showMenuOptions(): Boolean {
                            return false;
                        }

                        override fun showLockIcon(): Boolean {
                            return false
                        }

                        override fun showPreview(): Boolean {
                            return false
                        }
                } ) }

                result
            }

    // Methods

    fun setCurFolder(id: Long?) {
        mCurFolderId.value = id
    }

    fun setCurFolderUp() {
        mCurFolderId.value = mCurFolder.value?.ParentId
    }

    fun getCurFolder(): EncryptedFolder? {
        return mCurFolder.value
    }

    fun addEncryptedFolder(name: ByteArray, parent: EncryptedFolder?)
            = mRepository.addEncryptedFolder(name, parent)

    // Interface

    interface MoveListItem : ListItem {

        val id: Long
    }
}