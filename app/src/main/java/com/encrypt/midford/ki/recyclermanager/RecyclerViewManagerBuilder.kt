package com.encrypt.midford.ki.recyclermanager

import android.content.Context

import java.util.Comparator

class RecyclerViewManagerBuilder<T : ListItem>(context: Context) {

    val manager: RecyclerViewManager<T> = RecyclerViewManager(context)

    fun setComparator(comparator: Comparator<T>): RecyclerViewManagerBuilder<T> {
        manager.setComparator(comparator)
        return this
    }

    fun setEmptyStateText(r: Int): RecyclerViewManagerBuilder<T> {
        manager.setEmptyStateText(r)
        return this
    }

    fun startWithLoading(): RecyclerViewManagerBuilder<T> {
        manager.isLoading = true
        return this
    }

    fun setOnStateChangeListener(l: RecyclerViewManager.OnViewStateChangeListener):
            RecyclerViewManagerBuilder<T> {
        manager.setOnStateChangeListener(l)
        return this
    }

    fun setOnMenuItemSelectedListener(l: RecyclerViewManager.OnMenuItemSelectedListener<T>):
            RecyclerViewManagerBuilder<T> {
        manager.setOnMenuItemSelectedListener(l)
        return this
    }

    fun setOnListItemClickListener(l: RecyclerViewManager.OnListItemClickListener<T>):
            RecyclerViewManagerBuilder<T> {
        manager.setOnListItemClickedListener(l)
        return this
    }

    fun withBottomPadding(): RecyclerViewManagerBuilder<T> {
        manager.withBottomPadding()
        return this
    }

    fun enableSwipeToRefresh(): RecyclerViewManagerBuilder<T> {
        manager.enableSwipeToRefresh()
        return this
    }

    fun enableSelectionMode(): RecyclerViewManagerBuilder<T> {
        manager.enableSelectionMode()
        return this
    }
}
