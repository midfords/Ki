![Alt](/misc/images-ki/ki_feature_graphic.png "Banner")

# Ki
Ki is an easy to use, secure file encryption app. Keep all your sensitive files encrypted using your own password. With Ki, you can import, export, organize and search all of your sensitive files without compromising privacy. Ki can also securely display images directly in app.

Ki features:
* AES encryption.
* File organization with folders.
* Searching and sorting.
* Securely viewing encrypted images.

## Download and Installation
Ki can be found and installed from the Google Play Store [here](https://play.google.com/store/apps/details?id=com.encrypt.midford.ki "Title").

The product website can be found [here](https://ki.seanmidford.ca "Title").

## User Manual
### First Time Setup
Choose a password to use for encrypting and decrypting files. The password strength meter will help you pick a secure password. Currently, you can not change your password once picked. If you forget your password, there will be no way of recovering the files you've encrypted using Ki.

### Locking and Unlocking Ki
Unlock the app by entering your password into the log in screen. There are unlimited attempts to enter your password. Lock the app again by selecting the 'Lock' action from the app menu, or by simply closing the app.

### Importing and Exporting Files
Once Ki has been unlocked, you can import files by pressing the Floating Action Button and selecting any number of files to import.

> Note: There is a bug in some versions of Android that prevents apps from seeing every file that has been selected by a file selector. If this happens a warning message will be displayed on the import dialogue.

Export files by selecting the menu icon on individual files or folders, or by long-pressing and selecting multiple items at once. Items can also be removed from Ki automatically after exporting by selecting the checkbox on the export dialogue.

Everything will be exported as-is, including folder structures. Files are exported to:
   `/sdcard/Ki_export/`

### File Management
File management can be done on individual items or on groups of items. Manage individual items by pressing the menu button, then choosing an action. Manage groups of items by long-pressing on a list item and selecting multiple files and folders, then choosing an action from the main menu.

Files and folders can be renamed, moved and deleted from Ki using these actions.

## Screenshots

![Alt](/misc/images-ki/Screenshot_20190719-105902.png "Screenshot")

![Alt](/misc/images-ki/Screenshot_20190719-105914.png "Screenshot")

![Alt](/misc/images-ki/ki_screenshot_banner_crop.png "Feature")
